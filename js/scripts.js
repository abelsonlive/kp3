let scene, camera, render, kp;

// set control parameters
const backgroundColor = 0xdddddd;
const kpRotationFactor = 0.05;
const ambientLightColor = 0x404040;
const ambientLightIntensity = 100;
const directionalLightIntensity = 10;
const directionalLightColor = 0xffffff;

const cameraPositionX = 800;
const cameraPositionY = 100;
const cameraPositionZ = 1000;

function init() {
    // SCENE // 

    scene = new THREE.Scene()
    scene.background = new THREE.Color(backgroundColor);

    // LIGHTS // 

    hlight = new THREE.AmbientLight(ambientLightColor, ambientLightIntensity)
    scene.add(hlight)
    directionalLight = new THREE.DirectionalLight(directionalLightColor, directionalLightIntensity)
    directionalLight.position.set(0,1,0)
    directionalLight.castShadow = false
    scene.add(directionalLight)


    // CAMERA // 

    camera = new THREE.PerspectiveCamera(40, window.innerWidth/window.innerHeight,1,5000)
    camera.rotation.y = 45/180*Math.PI
    camera.position.x = cameraPositionX;
    camera.position.y = cameraPositionY;
    camera.position.z = cameraPositionZ;

    // ACTION // 

    renderer = new THREE.WebGLRenderer({antialias: true})
    renderer.setSize(window.innerWidth, window.innerHeight)
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    document.body.appendChild(renderer.domElement)

    // ACTOR // 

    let loader = new THREE.GLTFLoader()
    loader.load('../3d-obj-loader/assets/kpava.gltf', function(gltf){
        // access kp object and set global variable
        kp = gltf.scene.children[2];
        console.log(gltf.scene)
        kp.scale.set(3, 3, 3);
        kp.position.set(0,-250,0);
        directionalLight.lookAt(kp);
        scene.add(gltf.scene)
        animate();

    })
}

// ANIMATION LOOP
function animate() {
    renderer.render(scene,camera);
    // rotate kp
    kp.rotation.z += kpRotationFactor;
    requestAnimationFrame(animate);
  }
init()